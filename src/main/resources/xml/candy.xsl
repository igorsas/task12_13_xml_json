<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: yellow;
                    color: red;
                    text-align:right;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }

                </style>
            </head>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:20px">№</th>
                        <th style="width:150px">Name</th>
                        <th style="width:150px">energy</th>
                        <th style="width:150px">type</th>
                        <th style="width:150px">water</th>
                        <th style="width:150px">sugar</th>
                        <th style="width:150px">fructose</th>
                        <th style="width:150px">typeChocolate</th>
                        <th style="width:150px">vanillin</th>
                        <th style="width:150px">proteins</th>
                        <th style="width:150px">fats</th>
                        <th style="width:150px">carbs</th>
                        <th style="width:150px">production</th>
                    </tr>

                    <xsl:for-each select="candies/candy">

                        <tr>
                            <td class="colfmt">
                                <xsl:values-of select="@candyNumber"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="name"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="energy"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="type"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="ingredients/water"/>
                            </td>
                            <td class="colfmt">
                                <xsl:values-of select="ingredients/sugar"/>
                            </td>
                            <td class="colfmt">
                                <xsl:values-of select="ingredients/fructose"/>
                            </td>
                            <td class="colfmt">
                                <xsl:values-of select="ingredients/typeChocolate"/>
                            </td>
                            <td class="colfmt">
                                <xsl:values-of select="ingredients/vanillin"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="values/proteins"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="values/fats"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="values/carbs"/>
                            </td>

                            <td class="colfmt">
                                <xsl:values-of select="production"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>