package controller;

import model.comparator.CandyComparator;
import model.fileChecker.ExtensionChecker;
import model.tree.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public interface Controller {
    Logger logger = LogManager.getLogger(Controller.class);
    void execute();
    default void printList(List<Candy> candies) {
        candies.sort(new CandyComparator());
        for (Candy candy : candies) {
            logger.info(candy);
        }
    }
}
