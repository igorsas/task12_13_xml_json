package controller.jsonController;

import controller.Controller;
import model.parserJSON.JacksonParser;
import model.tree.Candy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static model.Constants.*;

public class JacksonController implements Controller{
    @Override
    public void execute() {
        try {
            File json = new File(JSON);
            List<Candy> candies = JacksonParser.getCandyList(json);
            printList(candies);
            logger.info(WRITE_TO_JSON);
            JacksonParser.writeCandyToJson(candies, PATH_FOR_WRITE_JSON_JACKSON);
            logger.info(DONE);
        } catch (FileNotFoundException e) {
            logger.error(FILE_NOT_FOUND_EXCEPTION);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(IO_EXCEPTION);
            e.printStackTrace();
        }
    }
}
