package controller.jsonController;

import controller.Controller;
import model.parserJSON.GSONParser;
import model.tree.Candy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import static model.Constants.*;

public class GSONController implements Controller {
    @Override
    public void execute() {
        try {
            List<Candy> candies = GSONParser.getCandyList(JSON);
            printList(candies);
            logger.info(WRITE_TO_JSON);
            GSONParser.writeCandyToJson(candies, PATH_FOR_WRITE_JSON_GSON);
            logger.info(DONE);
        } catch (FileNotFoundException e) {
            logger.error(FILE_NOT_FOUND_EXCEPTION);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(IO_EXCEPTION);
            e.printStackTrace();
        }
    }
}
