package controller.xmlController;

import controller.Controller;
import model.fileChecker.ExtensionChecker;

import java.io.File;

public interface XMLController extends Controller {
    default boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }
    default boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }
}
