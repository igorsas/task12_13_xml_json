package controller.xmlController;

import model.parserXML.dom.DOMParserUser;

import java.io.File;

import static model.Constants.XML;

public class DOMController implements XMLController {
    @Override
    public void execute() {
        File xml = new File(XML);
        if (checkIfXML(xml)) {
            printList(DOMParserUser.getCandyList(xml));
        }
    }
}
