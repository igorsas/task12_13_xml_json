import view.MyView;

public class Main {
    public static void main(String[] args) {
        MyView view = new MyView();
        view.show();
    }
}
