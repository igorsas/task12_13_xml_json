package view;

import controller.Controller;
import controller.jsonController.GSONController;
import controller.jsonController.JacksonController;
import controller.xmlController.DOMController;
import controller.xmlController.SAXController;
import controller.xmlController.StAXController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static model.Constants.*;

public class MyView {
    private Logger logger = LogManager.getLogger(MyView.class);
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;
    private static BufferedReader reader;

    public MyView() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        List<Controller> controllers = new ArrayList<>();
        controllers.add(new DOMController());
        controllers.add(new SAXController());
        controllers.add(new StAXController());
        controllers.add(new GSONController());
        controllers.add(new JacksonController());
        setMenu();
        setMethodsMenu(controllers);
    }

    private void setMethodsMenu(List<Controller> controllers) {
        methodsMenu = new LinkedHashMap<>();
        for (int i = 0; i < controllers.size(); i++) {
            methodsMenu.put(String.valueOf(i+1), controllers.get(i));
        }
    }

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", RUN_DOM);
        menu.put("2", RUN_SAX);
        menu.put("3", RUN_StAX);
        menu.put("4", RUN_GSON);
        menu.put("5", RUN_JACKSON);
    }

    private void outputMenu() {
        logger.info(MENU_MSG);
        for (String key : menu.keySet()) {
            if (key.length() == LENGTH_OF_POINT) {
                logger.info(key + " - " + menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu = QUIT_FROM_MENU;
        do {
            outputMenu();
            logger.info(ASK_WITH_CONTROLLER_RUN);
            try {
                keyMenu = reader.readLine().toUpperCase();
                methodsMenu.get(keyMenu).execute();
            } catch (IOException e) {
                logger.error(IO_EXCEPTION);
                e.printStackTrace();
            }
        } while (!keyMenu.equals(QUIT_FROM_MENU));
    }
}
