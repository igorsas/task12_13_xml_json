package model.tree;

public class Ingredients {
    private double water;
    private double sugar;
    private double fructose;
    private String typeChocolate;
    private double vanillin;

    public Ingredients() {
    }

    public Ingredients(double water, double sugar, double fructose, String chocolate, double vanillin) {
        this.water = water;
        this.sugar = sugar;
        this.fructose = fructose;
        this.typeChocolate = chocolate;
        this.vanillin = vanillin;
    }

    public double getWater() {
        return water;
    }

    public void setWater(double water) {
        this.water = water;
    }

    public double getSugar() {
        return sugar;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public double getFructose() {
        return fructose;
    }

    public void setFructose(double fructose) {
        this.fructose = fructose;
    }

    public String getTypeChocolate() {
        return typeChocolate;
    }

    public void setTypeChocolate(String typeChocolate) {
        this.typeChocolate = typeChocolate;
    }

    public double getVanillin() {
        return vanillin;
    }

    public void setVanillin(double vanillin) {
        this.vanillin = vanillin;
    }

    @Override
    public String toString() {
        return "Ingredients{" +
                "water=" + water +
                ", sugar=" + sugar +
                ", fructose=" + fructose +
                ", chocolate='" + typeChocolate + '\'' +
                ", vanillin=" + vanillin +
                '}';
    }
}
