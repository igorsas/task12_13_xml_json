package model.tree;

import java.util.Objects;

public class Candy {
    private int candyNumber;
    private String name;
    private double energy;
    private String type;
    private Ingredients ingredients;
    private Values values;
    private String production;

    public Candy() {
    }

    public Candy(int candyNumber, String name, double energy, String type, Ingredients ingredients, Values values, String production) {
        this.candyNumber = candyNumber;
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.values = values;
        this.production = production;
    }

    public int getCandyNumber() {
        return candyNumber;
    }

    public void setCandyNumber(int candyNumber) {
        this.candyNumber = candyNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Candy)) return false;
        Candy candy = (Candy) o;
        return getCandyNumber() == candy.getCandyNumber() &&
                Double.compare(candy.getEnergy(), getEnergy()) == 0 &&
                Objects.equals(getName(), candy.getName()) &&
                Objects.equals(getType(), candy.getType()) &&
                Objects.equals(getIngredients(), candy.getIngredients()) &&
                Objects.equals(getValues(), candy.getValues()) &&
                Objects.equals(getProduction(), candy.getProduction());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCandyNumber(), getName(), getEnergy(), getType(), getIngredients(), getValues(), getProduction());
    }

    @Override
    public String toString() {
        return "Candy{" +
                "candyNumber=" + candyNumber +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", type='" + type + '\'' +
                ", ingredients=" + ingredients +
                ", values=" + values +
                ", production='" + production + '\'' +
                '}';
    }
}
