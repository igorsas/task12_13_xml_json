package model.tree;

public class Values {
    private double proteins;
    private double fats;
    private double carbs;

    public Values() {
    }

    public Values(double proteins, double fats, double carbs) {
        this.proteins = proteins;
        this.fats = fats;
        this.carbs = carbs;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    @Override
    public String toString() {
        return "Values{" +
                "proteins=" + proteins +
                ", fats=" + fats +
                ", carbs=" + carbs +
                '}';
    }
}
