package model;

public class Constants {
    public static final int LENGTH_OF_POINT = 1;

    public static final String QUIT_FROM_MENU = "Q";
    public static final String ASK_WITH_CONTROLLER_RUN = "Please, select menu point.";
    public static final String RUN_DOM = "Run DOM Controller";
    public static final String RUN_SAX = "Run SAX Controller";
    public static final String RUN_StAX = "Run StAX Controller";
    public static final String RUN_GSON = "Run GSON Controller";
    public static final String RUN_JACKSON = "Run Jackson Controller";
    public static final String WRITE_TO_JSON = "Write to the json file: ";
    public static final String DONE = "DONE!";

    public static final String MENU_MSG = "MENU:";

    public static final String IO_EXCEPTION = "IO Exception";
    public static final String PARSER_CONFIG_EXCEPTION = "Parser Configuration Exception";
    public static final String SAX_EXCEPTION = "SAX Exception";
    public static final String FILE_NOT_FOUND_EXCEPTION = "File Not Found Exception";

    public static final String JSON = "src\\main\\resources\\json\\candy.json";
    public static final String XML = "src\\main\\resources\\xml\\candy.xml";
    public static final String PATH_FOR_WRITE_JSON_GSON = "src\\main\\resources\\json\\rewriteCandyGSON.json";
    public static final String PATH_FOR_WRITE_JSON_JACKSON = "src\\main\\resources\\json\\rewriteCandyJackson.json";

    private Constants() {}
}
