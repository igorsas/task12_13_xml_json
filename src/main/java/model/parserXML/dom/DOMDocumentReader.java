package model.parserXML.dom;

import model.tree.Candy;
import model.tree.Ingredients;
import model.tree.Values;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocumentReader {
    public List<Candy> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Candy> candies = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("candy");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            Ingredients ingredients;
            Values values;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                candy.setCandyNumber(Integer.parseInt(element.getAttribute("candyNumber")));
                candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
                candy.setEnergy(Double.parseDouble(element.getElementsByTagName("energy").item(0).getTextContent()));
                candy.setType(element.getElementsByTagName("type").item(0).getTextContent());
                candy.setProduction(element.getElementsByTagName("production").item(0).getTextContent());

                ingredients = getIngredients(element.getElementsByTagName("ingredients"));
                values = getValues(element.getElementsByTagName("values"));

                candy.setIngredients(ingredients);
                candy.setValues(values);
                candies.add(candy);
            }
        }
        return candies;
    }

    private Ingredients getIngredients(NodeList nodes) {
        Ingredients ingredients = new Ingredients();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            ingredients.setTypeChocolate(element.getElementsByTagName("typeChocolate").item(0).getTextContent());
            ingredients.setFructose(Double.parseDouble(element.getElementsByTagName("fructose").item(0).getTextContent()));
            ingredients.setSugar(Double.parseDouble(element.getElementsByTagName("sugar").item(0).getTextContent()));
            ingredients.setVanillin(Double.parseDouble(element.getElementsByTagName("vanillin").item(0).getTextContent()));
            ingredients.setWater(Double.parseDouble(element.getElementsByTagName("water").item(0).getTextContent()));
        }
        return ingredients;
    }

    private Values getValues(NodeList nodes){
        Values chars = new Values();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            chars.setCarbs(Double.parseDouble(element.getElementsByTagName("carbs").item(0).getTextContent()));
            chars.setFats(Double.parseDouble(element.getElementsByTagName("fats").item(0).getTextContent()));
            chars.setProteins(Double.parseDouble(element.getElementsByTagName("proteins").item(0).getTextContent()));
        }

        return chars;
    }
}
