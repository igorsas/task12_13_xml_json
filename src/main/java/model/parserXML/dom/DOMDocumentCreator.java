package model.parserXML.dom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static model.Constants.*;

public class DOMDocumentCreator {
    private Logger logger = LogManager.getLogger(DOMDocumentCreator.class);
    private DocumentBuilder documentBuilder;
    private Document document;

    public DOMDocumentCreator(File xml) {
        createDOMBuilder();
        createDoc(xml);
    }

    private void createDOMBuilder() {
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error(PARSER_CONFIG_EXCEPTION);
            e.printStackTrace();
        }
    }

    private void createDoc(File xml) {
        try {
            document = documentBuilder.parse(xml);
        } catch (SAXException e) {
            logger.error(SAX_EXCEPTION);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error(IO_EXCEPTION);
            e.printStackTrace();
        }
    }

    public Document getDocument() {
        return document;
    }
}
