package model.parserXML.dom;

import model.tree.Candy;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class DOMParserUser {
    public static List<Candy> getCandyList(File xml){
        DOMDocumentCreator creator = new DOMDocumentCreator(xml);
        Document doc = creator.getDocument();

        DOMDocumentReader reader = new DOMDocumentReader();

        return reader.readDoc(doc);
    }
}
